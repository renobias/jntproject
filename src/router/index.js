import { createStackNavigator,CardStyleInterpolators,TransitionPresets } from '@react-navigation/stack';
import React from 'react';
import {PackageTracing,WaybillPress} from '../Pages'
import { StyleSheet, Text, View,Button, ImageBackground } from 'react-native';

export default class Router extends React.Component{
        constructor(props){
            super(props);
        }

        render(){
            const Stack = createStackNavigator();
            return(
                <Stack.Navigator initialRouteName="PackageTracing">
                    <Stack.Screen name="PackageTracing" component={PackageTracing} options={({navigation})=>({
                        headerStyle:{
                            backgroundColor:"red",
                            elevation:0,
                            shadowOpacity:0,
                            borderBottomWidth:0
                        },
                        title:"Lacak Paket",
                        headerTintColor: "white",
                        headerTitleStyle:{
                            fontFamily:"arial",
                            fontWeight:"bold",
                            textAlign:"center"
                        }
                    })}>    
                    </Stack.Screen>
                    <Stack.Screen name="WaybillPress" component={WaybillPress} options={({navigation})=>({
                        headerStyle:{
                            backgroundColor:"red",
                            elevation:0,
                            shadowOpacity:0,
                            borderBottomWidth:0
                        },
                        title:"Waybill Press",
                        headerTintColor: "white",
                        headerTitleStyle:{
                            fontFamily:"arial",
                            fontWeight:"bold",
                            textAlign:"center"
                        }
                    })}>    
                    </Stack.Screen>
                </Stack.Navigator>
            )
        }
    }
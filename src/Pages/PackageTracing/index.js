import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Image,
  Dimensions
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import { Value } from 'react-native-reanimated';
import { connect } from 'react-redux';
import {setBgColorButton,setinputWaybill,setselectedId,addDATATEXTINPUTWAYBILL} from '../../redux'
import {widthPercentageToDP as wp,widthPercentageToDP as hp} from 'react-native-responsive-screen'

class PackageTracing extends React.Component{
        constructor(props,{navigation,route}){
            super(props)

            // this.navigation = navigation;
            // this.route = route;

            // this.state = {bgColorBtn:"white",selectedId:null,inputWaybill:""}

            this.DATA = [
                {
                  id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
                  title: "First Item",
                  waybill:"1051444635"
                },
                {
                  id: "3ac68afc-c605-48d3-a4f8-fbd91aa97f63",
                  title: "Second Item",
                  waybill:"1051444671"
                },
                {
                  id: "58694a0f-3da1-471f-bd96-145571e29d72",
                  title: "Third Item",
                  waybill:"1051444671"
                },
              ];
            
            //   this.DATATEXTINPUTWAYBILL = [
            //   ];

              this.navToWaybillSelect = this.navToWaybillSelect.bind(this);
              this.getWayBill = this.getWayBill.bind(this);
              this.HistoryToInput = this.HistoryToInput.bind(this);
        }

        changeBgColorButton(){
            if(this.props.bgColorBtn==="white"){
                this.props.setBgColorButton("#dddddd")
                // this.setState({bgColorBtn:"#dddddd"})
            }else{
                this.props.setBgColorButton("white")
                // this.setState({bgColorBtn:"white"})
            }
        }

        navToWaybillSelect = (waybill) => {
            this.props.navigation.navigate('WaybillPress',{selectedWaybill:waybill})
        }

        getWayBill = (val) =>{
            var objWB = {id:'58694a0f-3da1-471f-bd96-145571e29d72',
                title:'',
                waybill:''};
            if(val.includes(" ")||val.includes(",")){
                if(this.props.PackageTracing.DATATEXTINPUTWAYBILL.length>9){
                    this.props.setinputWaybill(val)
                    // this.setState({inputWaybill:val})
                }else{
                val=val.substring(0,val.length-1)
                objWB.waybill = val;
                this.props.setinputWaybill("")
                this.props.addDATATEXTINPUTWAYBILL(objWB)
                //this.props.PackageTracing.DATATEXTINPUTWAYBILL.push(objWB)
                //this.setState({inputWaybill:""})
                objWB=''
                }
            }
            else{
                this.props.setinputWaybill(val)
                // this.setState({inputWaybill:val})
            }
        }

        HistoryToInput = (waybill) =>{
            var objWBB = {id:'58694a0f-3da1-471f-bd96-145571e29d72',
                title:'',
                waybill:''};

            objWBB.waybill=waybill;
            this.props.addDATATEXTINPUTWAYBILL(objWBB)
            //this.props.PackageTracing.DATATEXTINPUTWAYBILL.push(objWBB);
            this.props.setinputWaybill("")
            //this.setState({inputWaybill:""})
            // this.setState({selectedId:waybill})

            console.log(objWBB.waybill)
            console.log(this.props.PackageTracing.DATATEXTINPUTWAYBILL)
        }

        render(){

            const Item = ({ item, onPress, style }) => (
                <TouchableOpacity style={[styles.item, style]} onPress={()=>this.HistoryToInput(item.waybill)}>
                  <Text style={styles.title}>{item.waybill}</Text>
                </TouchableOpacity>
              );

            const ItemTextInputWaybill = ()=>{
                return(
                        <View style={{flex:1,flexDirection:"row",flexWrap:"wrap",width:("100%")}}>
                            {this.props.PackageTracing.DATATEXTINPUTWAYBILL.map((y,z)=>{
                                return(
                                <View style={{flexGrow:1,width:("40%"),marginTop:10,marginLeft:10}}>
                                    <TouchableOpacity onPress={()=>this.navToWaybillSelect(y.waybill.toString())} style={{backgroundColor:"#dddddd",borderRadius:10,padding:3}}>
                                        <Text style={{fontSize:hp("3%")}}>{y.waybill.toString()}</Text>
                                    </TouchableOpacity>
                                </View>
                                )
                            })}

                            {this.props.PackageTracing.DATATEXTINPUTWAYBILL.length<=9 ? 
                            <View style={{flexGrow:1,width:("40%"),marginLeft:10}}>
                                <TextInput
                                    multiline
                                    placeholder="Maksimum track 10 waybill, dapat dipisah dengan tanda koma, spasi atau enter"
                                    placeholderTextColor="#dddddd"
                                    style={styles.textTracking}
                                    value={this.props.PackageTracing.inputWaybill}
                                    onChangeText={val => this.getWayBill(val)}
                                >
                                </TextInput>
                            </View> : null}
                        </View>
                )
            }

            const renderItem = ({ item }) => {
            const backgroundColor = item.waybill === this.props.selectedId ? "black" : "white";
            //const backgroundColor = "white";
        
            return (
                <Item
                item={item}
                style={{ backgroundColor}}
                />
            );
            };
        
            return(
                <View style={styles.container}>
                    <View style={styles.containerTextSeacrh}>
                        <View style={{height:"50%",width:"100%"}}>
                            <View style={{height:"60%",width:"22%",alignSelf:"center"}}>
                                <Image 
                                    style={{width:"100%",height:"100%"}} 
                                    source={require('../../img/qr-code.png')}/>
                            </View>
                            <View style={{height:"40%"}}>
                                <Text style={{color:"white",alignSelf:"center",fontSize:hp("4%"),marginTop:"3%"}}>Scan</Text>
                            </View>
                        </View>
                        <View style={styles.textTrackingWrapper}>
                            <View style={{height:("80%")}}>
                                <ScrollView style={{height:"100%"}}>
                                    {ItemTextInputWaybill()}
                                </ScrollView>
                            </View>

                            <View style={styles.line}/>
                            <TouchableOpacity 
                                    style={{height:"40%",backgroundColor:"white",width:("100%"),justifyContent:"center"}} 
                                    >
                                    <Text style={{color:"red",alignSelf:"center",fontSize:hp("3%")}}>
                                        Cari
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <SafeAreaView style={styles.containerHistorySeacrh}>
                        <FlatList
                            data={this.DATA}
                            renderItem={renderItem}
                            keyExtractor={(item) => item.waybill}
                            extraData={this.props.PackageTracing.selectedId}
                        />
                    </SafeAreaView>
                </View>
            )
        }
    }

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    containerTextSeacrh:{
       height:"40%",
       backgroundColor:"red",
       padding:20,
    },
    containerHistorySeacrh:{
        height:"60%",
        paddingHorizontal:20,
        backgroundColor:"white"
    },
    textTrackingWrapper:{
        height:("50%"),
        backgroundColor:"white",
        paddingTop:10,
        paddingLeft:20,
        paddingRight:20,
        color:"gray",
        fontSize:hp("3%"),
        fontFamily:"helvetica",
        borderRadius:5
        // backgroundColor:"yellow"
    },
    textTracking:{
        padding:0,
        textAlignVertical:"top",
        fontSize:hp("3%"),
        borderColor:"white"
    },
    line:{
        borderTopColor:"red",
        borderTopWidth:1,
        alignItems:"center",
        height:"0%",
        width:"100%"
    },
    item: {
        padding: 10,
        borderBottomWidth:1,
        borderBottomColor:"#dddddd"
      },
     title: {
        fontSize: 12,
      },
})

const mapStateToProps = state =>{
    return {
        PackageTracing:state.reducerPackageTracing
    }
}

const mapDispatchToProps = () => {
    return{
        setBgColorButton,setselectedId,setinputWaybill,addDATATEXTINPUTWAYBILL
    };
};

export default connect (
    mapStateToProps,
    mapDispatchToProps()
)(PackageTracing)